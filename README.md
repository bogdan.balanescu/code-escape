# Code Escape

Date of development: Mar 2017 - Jul 2017

3D programming game, where the player must find a hidden item in a randomly generated maze.
Blue and green doors are locked.
Blue doors open programming related quizes, while green doors open problems which must be solved by writing an algorithm.
The languages in which the player must write the algorithms is developed using the UML diagrams found in the "Diagrams" folder.
Language: C#
Framework: .NET 4.5

Source code not made public yet.

In the making of this project, a 3rd party package was used: Invector-3rdPersonController by Invector